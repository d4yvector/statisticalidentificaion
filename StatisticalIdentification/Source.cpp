//色検出プログラム
//キーボード入力があるとプログラムを終了するようになっています
#include <opencv2/opencv.hpp>
#include <iostream>

//入力画像ファイル名
#define FILENAME "sample.jpg"

//入力映像サイズ（自動的に設定される）
int img_WIDTH;
int img_HEIGHT;

typedef struct {
	int flag;
	int x[2];
	int y[2];
} dinfo;
dinfo drag;		 //マウスドラッグの状態

cv::Mat frame;		 //画像保存用
int COLOR_P = 0;	 //色の検出をするか否かのフラグ
#define LUTsiz 256	 //LookUpTableの大きさ
					 //LUT
uchar TLUT[LUTsiz][LUTsiz][LUTsiz];//ターゲットIDを格納
uchar DLUT[LUTsiz][LUTsiz][LUTsiz];//ターゲットとの距離を格納
								   /////////////////////////////////////////////////////////////////////////////

								   //LUT内の距離変換計算
void extend()
{
	//この関数を埋める．
}

///////////////////////// onButton() から呼び出される関数群(始まり)
//選択領域の初期化
void drag_init()
{
	drag.flag = 0;
	drag.x[0] = -1;
	drag.x[1] = -1;
	drag.y[0] = -1;
	drag.y[1] = -1;
}

//選択領域のRGB値をLUTにidの色として登録
void get_rgb(uchar id)
{
	for (int i = drag.x[0]; i <= drag.x[1]; i++) {
		for (int j = drag.y[0]; j <= drag.y[1]; j++) {
			cv::Vec3b pixel = frame.at<cv::Vec3b>(j, i);
			//Lookup table(R,G,B)のタグ(ターゲットIDと距離)を変更
			uchar red = pixel[2];
			uchar green = pixel[1];
			uchar blue = pixel[0];
			TLUT[(uint)red][(uint)green][(uint)blue] = id;
			DLUT[(uint)red][(uint)green][(uint)blue] = 0;
		}
	}
}

//LUTの初期化
void lut_init()
{
	for (int i = 0; i < LUTsiz; i++) {
		for (int j = 0; j < LUTsiz; j++) {
			for (int k = 0; k < LUTsiz; k++) {
				TLUT[i][j][k] = 0;
				DLUT[i][j][k] = 255;
			}
		}
	}
}
///////////////////////// onButton() から呼び出される関数群(終わり)


///////////////////ボタン処理 onMouse() の中から呼び出される．
void onButton(int x, int y)
{
	int button_id = -1;
	//ボタンの左列をクリック
	if (x < img_WIDTH + 100) {
		for (int i = 0; i < 8; i++) {
			if (y > i*(img_HEIGHT / 8) && y < (i + 1)*(img_HEIGHT / 8))
				button_id = i;
		}
	}
	//ボタンの右列をクリック
	else {
		for (int i = 0; i < 8; i++) {
			if (y > i*(img_HEIGHT / 8) && y < (i + 1)*(img_HEIGHT / 8))
				button_id = i + 8;
		}
	}

	std::cout << "ボタン" << button_id << "が押されました。" << std::endl;
	if (button_id >= 0) {
		switch (button_id) {
		case 0:
		case 1:
			//選択領域をLUTに登録
			if (drag.x[0] != -1) {
				get_rgb(button_id);
				std::cout << "選択領域のRGB情報をLUTに登録しました。" << std::endl;
				extend();
				drag_init();
			}
			break;
			//右のボタン
		case 8:
			//LUT初期化
			lut_init();
			break;
		case 9: //色検出のon/off
			if (COLOR_P == 0) COLOR_P = 1;
			else if (COLOR_P == 1) COLOR_P = 0;
			break;
		}
	}
}

//マウスイベント取得関数///ドラッグとボタン操作の検出
void onMouse(int event, int x, int y, int flags, void*)
{
	//マウス左ボタンが押された
	if (event == CV_EVENT_LBUTTONDOWN) {
		//ボタン領域をクリックした場合
		if (x >= img_WIDTH) {
			onButton(x, y);
			return;
		}
		//画像領域をクリックした場合
		drag.flag = 1;
		//マウスドラッグの始点を保存
		drag.x[0] = x;
		drag.y[0] = y;
		//始点が画面外だった場合
		if (drag.x[0] < 0) drag.x[0] = 0;
		if (drag.x[0] >= img_WIDTH) drag.x[0] = img_WIDTH - 1;
		if (drag.y[0] < 0) drag.y[0] = 0;
		if (drag.y[0] >= img_HEIGHT) drag.y[0] = img_HEIGHT - 1;
	}

	//マウス左ボタンが離された
	if (event == CV_EVENT_LBUTTONUP) {
		if (drag.flag == 1) {
			drag.flag = 0;
			//マウスドラッグの終点を保存
			drag.x[1] = x;
			drag.y[1] = y;
			//終点が画面外だった場合
			if (drag.x[1] < 0) drag.x[1] = 0;
			if (drag.x[1] >= img_WIDTH) drag.x[1] = img_WIDTH - 1;
			if (drag.y[1] < 0) drag.y[1] = 0;
			if (drag.y[1] >= img_HEIGHT) drag.y[1] = img_HEIGHT - 1;

			//マウスドラッグの終点がボタン領域の場合
			if (drag.x[1] >= img_WIDTH) drag.x[1] = img_WIDTH - 1;

			//x[0]<x[1], y[0]<y[1]となるように入れ替え
			int tmp;
			if (drag.x[0] > drag.x[1]) {
				tmp = drag.x[0];
				drag.x[0] = drag.x[1];
				drag.x[1] = tmp;
			}
			if (drag.y[0] > drag.y[1]) {
				tmp = drag.y[0];
				drag.y[0] = drag.y[1];
				drag.y[1] = tmp;
			}

			//マウスドラッグの範囲を画面出力
			std::cout << "[" << drag.x[0] << "," << drag.y[0] << "] → [" << drag.x[1] << "," << drag.y[1] << "]" << std::endl;
		}
	}

	//マウスドラッグ中はマウスドラッグの終点を動的に変化させる
	if (drag.flag == 1) {
		drag.x[1] = x;
		drag.y[1] = y;
	}
}
///////////////////////////////////////////////////////////////

///////ドラッグして選択した領域に短形を描画(mainから呼び出される)
void draw_rectangle(cv::Mat output)
{
	cv::Point2i p1(drag.x[0], drag.y[0]), p2(drag.x[1], drag.y[1]);
	cv::rectangle(output, p1, p2, cv::Scalar(255, 255, 255), 2, 4);
}

//////画像上で指定した色として認識された色を塗りつぶし処理する
//////mainから呼び出される．
void color_p(cv::Mat output)
{
	//色検出時の塗りつぶし色
	static int color[][3] = { { 0, 0, 255 },{ 0, 255, 0 },{ 0, 255, 255 },
	{ 255, 0, 0 },{ 255, 0, 255 },{ 255, 255, 0 },{ 255, 255, 255 } };
	for (int i = 0; i < img_WIDTH; i++) {
		for (int j = 0; j < img_HEIGHT; j++) {
			cv::Vec3b pixel = output.at<cv::Vec3b>(j, i);
			uchar red = pixel[2];
			uchar green = pixel[1];
			uchar blue = pixel[0];
			int id = TLUT[(uint)red][(uint)green][(uint)blue];
			if (id > 0) {
				pixel[0] = color[id - 1][0];
				pixel[1] = color[id - 1][1];
				pixel[2] = color[id - 1][2];
				output.at<cv::Vec3b>(j, i) = pixel;
			}
		}
	}
}

int main()
{
	cv::Mat output;//出力用
				   ///////////////////////////////ボタン画像読み込み
	cv::Mat button = cv::imread("./BUTTON1.JPG", 1);
	cv::Mat button_2 = cv::imread("./BUTTON2.JPG", 1);

	lut_init();//lutの初期化

			   ///////////画像入力
	frame = cv::imread(FILENAME);
	//入力画像サイズの取得
	img_WIDTH = frame.cols;
	img_HEIGHT = frame.rows;
	std::cout << "入力映像サイズ:" << img_WIDTH << "x" << img_HEIGHT << std::endl;

	//ボタンサイズを適切にスケーリング
	double resize_y = (double)img_HEIGHT / (double)480;
	cv::resize(button, button, cv::Size(), 1, resize_y);
	cv::resize(button_2, button_2, cv::Size(), 1, resize_y);

	cv::namedWindow("Image", cv::WINDOW_AUTOSIZE);
	/////////////////onMouse()をImageというウインドウのcall back関数として登録
	cv::setMouseCallback("Image", onMouse);
	drag_init();
	while (1)
	{
		frame.copyTo(output);
		if (COLOR_P == 1) color_p(output);
		draw_rectangle(output);

		cv::hconcat(output, button, output);//ボタン画像と入力映像を横に連結
		cv::hconcat(output, button_2, output);//ボタン画像2と入力映像を横に連結
		cv::imshow("Image", output);

		if (cv::waitKey(1) >= 0) break; //キーボード入力の検出
	}
}
