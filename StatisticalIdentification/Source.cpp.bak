
#include <stdio.h>
#include <stdlib.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <opencv2\opencv.hpp>

using namespace std;
using namespace cv;

#define IRIS_FNAME "fishers_iris_.txt"
#define DATA_NUM 50

Mat Average(Mat *X, int dataNum);
Mat Covariance(Mat *X, int dataNum);
double ProbabilityDensityFunc(Mat X, int dimNum, Mat Cov, Mat XAvg);
void StatisticalIdentification(Mat* X, int dataNum, int labelNum, Mat *LabelAvgs, Mat *LabelCovs, int *classification);
void ReadIrisData(Mat* setosa, Mat* verginica, Mat* versicolor, int flowerLabels[DATA_NUM * 3], Mat* sequentialPtr[DATA_NUM * 3]);
void ReadIris2DData(Mat* setosa, Mat* verginica, Mat* versicolor, int flowerLabels[DATA_NUM * 3], Mat* sequentialPtr[DATA_NUM * 3]);
void ReadIris3DData(Mat* setosa, Mat* verginica, Mat* versicolor, int flowerLabels[DATA_NUM * 3], Mat* sequentialPtr[DATA_NUM * 3]);

const char *flowerName[3] = {"Setosa", "Verginica", "Versicolor"};

int main(void)
{
	// アイリスデータの読み込み
	Mat setosa[DATA_NUM], verginica[DATA_NUM], versicolor[DATA_NUM];
	Mat* irisData[3] = {setosa, verginica, versicolor};
	int flowerLabels[DATA_NUM * 3];
	Mat* sequentialIrisDataPtr[DATA_NUM * 3];
	ReadIrisData(setosa, verginica, versicolor, flowerLabels, sequentialIrisDataPtr);

	// 2Dデータの読み込み
	Mat setosa2D[DATA_NUM], verginica2D[DATA_NUM], versicolor2D[DATA_NUM];
	Mat* iris2DData[3] = {setosa2D, verginica2D, versicolor2D};
	int flower2DLabels[DATA_NUM * 3];
	Mat* sequentialIris2DDataPtr[DATA_NUM * 3];
	ReadIris2DData(setosa2D, verginica2D, versicolor2D, flower2DLabels, sequentialIris2DDataPtr);

	// 3Dデータの読み込み
	Mat setosa3D[DATA_NUM], verginica3D[DATA_NUM], versicolor3D[DATA_NUM];
	Mat* iris3DData[3] = {setosa3D, verginica3D, versicolor3D};
	int flower3DLabels[DATA_NUM * 3];
	Mat* sequentialIris3DDataPtr[DATA_NUM * 3];
	ReadIris3DData(setosa3D, verginica3D, versicolor3D, flower3DLabels, sequentialIris3DDataPtr);
	
	// 平均ベクトルを求める
	Mat irisDataAvg[3], iris2DDataAvg[3], iris3DDataAvg[3];
	for (int i = 0; i < 3; i++) irisDataAvg[i] = Average(irisData[i], DATA_NUM);
	for (int i = 0; i < 3; i++) iris2DDataAvg[i] = Average(iris2DData[i], DATA_NUM);
	for (int i = 0; i < 3; i++) iris3DDataAvg[i] = Average(iris3DData[i], DATA_NUM);

	// 分散共分散行列を求める
	Mat irisDataCov[3], iris2DDataCov[3], iris3DDataCov[3];
	for (int i = 0; i < 3; i++) irisDataCov[i] = Covariance(irisData[i], DATA_NUM);
	for (int i = 0; i < 3; i++) iris2DDataCov[i] = Covariance(iris2DData[i], DATA_NUM);
	for (int i = 0; i < 3; i++)	iris3DDataCov[i] = Covariance(iris3DData[i], DATA_NUM);

	// 分類する
	int classification[DATA_NUM * 3];
	Mat sequentialIrisData[DATA_NUM * 3];	// 判定するデータ
	for (int i = 0; i < DATA_NUM * 3; i++) sequentialIrisData[i] = *(sequentialIrisDataPtr[i]);
	StatisticalIdentification(sequentialIrisData, DATA_NUM * 3, 3, irisDataAvg, irisDataCov, classification);

	int classification2D[DATA_NUM * 3];
	Mat sequentialIris2DData[DATA_NUM * 3];	// 判定するデータ
	for (int i = 0; i < DATA_NUM * 3; i++) sequentialIris2DData[i] = *(sequentialIris2DDataPtr[i]);
	StatisticalIdentification(sequentialIris2DData, DATA_NUM * 3, 3, iris2DDataAvg, iris2DDataCov, classification2D);

	int classification3D[DATA_NUM * 3];
	Mat sequentialIris3DData[DATA_NUM * 3];	// 判定するデータ
	for (int i = 0; i < DATA_NUM * 3; i++) sequentialIris3DData[i] = *(sequentialIris3DDataPtr[i]);
	StatisticalIdentification(sequentialIris3DData, DATA_NUM * 3, 3, iris3DDataAvg, iris3DDataCov, classification3D);

	// Confusion Matrixを求める
	double confusion[3][3] = {0.0};
	for (int i = 0; i < DATA_NUM * 3; i++)
		confusion[flowerLabels[i]][classification[i]]++;
	for (int flower = 0; flower < 3; flower++)
		for (int i = 0; i < 3; i++) confusion[flower][i] /= (double)DATA_NUM;
	printf("IrisData Confusion Matrix\n");
	printf("%lf %lf %lf\n", confusion[0][0], confusion[0][1], confusion[0][2]);
	printf("%lf %lf %lf\n", confusion[1][0], confusion[1][1], confusion[1][2]);
	printf("%lf %lf %lf\n\n", confusion[2][0], confusion[2][1], confusion[2][2]);

	double confusion2D[3][3] = {0.0};
	for (int i = 0; i < DATA_NUM * 3; i++)
		confusion2D[flower2DLabels[i]][classification2D[i]]++;
	for (int flower = 0; flower < 3; flower++) {
		confusion2D[flower][0] /= (double)DATA_NUM;
		confusion2D[flower][1] /= (double)DATA_NUM;
		confusion2D[flower][2] /= (double)DATA_NUM;
	}
	printf("Iris2DData Confusion Matrix\n");
	printf("%lf %lf %lf\n", confusion2D[0][0], confusion2D[0][1], confusion2D[0][2]);
	printf("%lf %lf %lf\n", confusion2D[1][0], confusion2D[1][1], confusion2D[1][2]);
	printf("%lf %lf %lf\n\n", confusion2D[2][0], confusion2D[2][1], confusion2D[2][2]);

	double confusion3D[3][3] = {0.0};
	for (int i = 0; i < DATA_NUM * 3; i++)
		confusion3D[flower3DLabels[i]][classification3D[i]]++;
	for (int flower = 0; flower < 3; flower++) {
		confusion3D[flower][0] /= (double)DATA_NUM;
		confusion3D[flower][1] /= (double)DATA_NUM;
		confusion3D[flower][2] /= (double)DATA_NUM;
	}
	printf("Iris3DData Confusion Matrix\n");
	printf("%lf %lf %lf\n", confusion3D[0][0], confusion3D[0][1], confusion3D[0][2]);
	printf("%lf %lf %lf\n", confusion3D[1][0], confusion3D[1][1], confusion3D[1][2]);
	printf("%lf %lf %lf\n\n", confusion3D[2][0], confusion3D[2][1], confusion3D[2][2]);

	// データごとの事後確率をファイルに出力
	FILE *fout = fopen("result.txt", "w");
	if (fout == NULL) { printf("can not open file\n"); exit(1); }
	fprintf(fout, "IrisData Confusion Matrix\n");
	fprintf(fout, "%lf %lf %lf\n", confusion[0][0], confusion[0][1], confusion[0][2]);
	fprintf(fout, "%lf %lf %lf\n", confusion[1][0], confusion[1][1], confusion[1][2]);
	fprintf(fout, "%lf %lf %lf\n\n", confusion[2][0], confusion[2][1], confusion[2][2]);
	fprintf(fout, "Iris2DData Confusion Matrix\n");
	fprintf(fout, "%lf %lf %lf\n", confusion2D[0][0], confusion2D[0][1], confusion2D[0][2]);
	fprintf(fout, "%lf %lf %lf\n", confusion2D[1][0], confusion2D[1][1], confusion2D[1][2]);
	fprintf(fout, "%lf %lf %lf\n\n", confusion2D[2][0], confusion2D[2][1], confusion2D[2][2]);
	fprintf(fout, "Iris3DData Confusion Matrix\n");
	fprintf(fout, "%lf %lf %lf\n", confusion3D[0][0], confusion3D[0][1], confusion3D[0][2]);
	fprintf(fout, "%lf %lf %lf\n", confusion3D[1][0], confusion3D[1][1], confusion3D[1][2]);
	fprintf(fout, "%lf %lf %lf\n\n", confusion3D[2][0], confusion3D[2][1], confusion3D[2][2]);
		
	fclose(fout);

	system("pause");
	return 0;
}

Mat Average(Mat *X, int dataNum)
{
	Mat avg = Mat::zeros(X->rows, X->cols, CV_32FC1);
	for (int i = 0; i < dataNum; i++) avg += X[i];
	avg /= dataNum;
	return avg;
}

Mat Covariance(Mat *X, int dataNum)
{
	Mat Cov, A_, avg;
	avg = Average(X, dataNum);
	Cov = Mat::zeros(X->rows, X->rows, CV_32FC1);
	A_ = Mat::zeros(X->rows, X->rows, CV_32FC1);
	for (int i = 0; i < dataNum; i++) {
		A_ = X[i] - avg;
		Cov += A_ * A_.t();
	}
	Cov /= DATA_NUM;
	
	return Cov;
}

void StatisticalIdentification(Mat* X, int dataNum, int labelNum, Mat *LabelAvgs, Mat *LabelCovs, int *classification)
{
	double maxVal;
	int maxIndex;
	for (int i = 0; i < dataNum; i++) {
		maxVal = ProbabilityDensityFunc(X[i], X->rows, LabelCovs[0], LabelAvgs[0]);
		maxIndex = 0;
		for (int label = 1; label < labelNum; label++) {
			if (ProbabilityDensityFunc(X[i], X->rows, LabelCovs[label], LabelAvgs[label]) > maxVal) {
				maxVal = ProbabilityDensityFunc(X[i], X->rows, LabelCovs[label], LabelAvgs[label]);
				maxIndex = label;
			}
		}
		classification[i] = maxIndex;
	}
}

double ProbabilityDensityFunc(Mat X, int dimNum, Mat Cov, Mat XAvg)
{
	return 1.0 / (pow(2.0 * M_PI, dimNum / 2.0) * determinant(Cov))
		* exp((-1.0 / 2.0) * (X - XAvg).dot(Cov.inv() * ((X - XAvg))));
}

void ReadIrisData(Mat* setosa, Mat* verginica, Mat* versicolor, int flowerLabels[DATA_NUM * 3], Mat* sequentialPtr[DATA_NUM * 3])
{
	FILE *fp;
	fp = fopen(IRIS_FNAME, "r");
	if (fp == NULL) { printf("can not open file\n"); exit(1); }

	Mat* X[3] = {setosa, verginica, versicolor};	// 取得したアイリスデータ
	int x[4];										// 一時的にファイルから読み込んだ値
	int dataNum[3] = {0};
	for (int i = 0; i < DATA_NUM * 3; i++) {
		fscanf(fp, "%d %d %d %d %d", &flowerLabels[i], &x[0], &x[1], &x[2], &x[3]);
		flowerLabels[i]--;	// 花の番号1~3を0~2へ変換

		// ベクトルデータへのポインタを記憶
		sequentialPtr[i] = &(X[flowerLabels[i]][dataNum[flowerLabels[i]]]);

		// 花のデータを列ベクトルに代入
		X[flowerLabels[i]][dataNum[flowerLabels[i]]] = cv::Mat(4, 1, CV_32FC1);
		X[flowerLabels[i]][dataNum[flowerLabels[i]]].at<float>(0, 0) = (float)x[0];
		X[flowerLabels[i]][dataNum[flowerLabels[i]]].at<float>(1, 0) = (float)x[1];
		X[flowerLabels[i]][dataNum[flowerLabels[i]]].at<float>(2, 0) = (float)x[2];
		X[flowerLabels[i]][dataNum[flowerLabels[i]]].at<float>(3, 0) = (float)x[3];
		dataNum[flowerLabels[i]]++;
	}
	fclose(fp);
}

void ReadIris2DData(Mat* setosa, Mat* verginica, Mat* versicolor, int flowerLabels[DATA_NUM * 3], Mat* sequentialPtr[DATA_NUM * 3])
{
	FILE *fp;
	const char *fileBaseName = "result_2d_";
	const char *fileExtension = ".csv";
	char fname[256];
	for (int flower = 0; flower < 3; flower++) {
		sprintf(fname, "%s%s%s", fileBaseName, flowerName[flower], fileExtension);
		fp = fopen(fname, "r");
		if (fp == NULL) { printf("can not open file\n"); exit(1); }

		Mat* X[3] = {setosa, verginica, versicolor};	// 取得したアイリスデータ
		float x[2];									// 一時的にファイルから読み込んだ値
		for (int i = 0; i < DATA_NUM; i++) {
			fscanf(fp, "%f, %f", &x[0], &x[1]);

			flowerLabels[i + (flower * 50)] = flower;

			// ベクトルデータへのポインタを記憶
			sequentialPtr[i + (flower * 50)] = &(X[flower][i]);

			// 花のデータを列ベクトルに代入
			X[flower][i] = cv::Mat(2, 1, CV_32FC1);
			X[flower][i].at<float>(0, 0) = (float)x[0];
			X[flower][i].at<float>(1, 0) = (float)x[1];
		}
		fclose(fp);
	}
}

void ReadIris3DData(Mat* setosa, Mat* verginica, Mat* versicolor, int flowerLabels[DATA_NUM * 3], Mat* sequentialPtr[DATA_NUM * 3])
{
	FILE *fp;
	const char *fileBaseName = "result_3d_";
	const char *fileExtension = ".csv";
	char fname[256];
	for (int flower = 0; flower < 3; flower++) {
		sprintf(fname, "%s%s%s", fileBaseName, flowerName[flower], fileExtension);
		fp = fopen(fname, "r");
		if (fp == NULL) { printf("can not open file\n"); exit(1); }

		Mat* X[3] = {setosa, verginica, versicolor};	// 取得したアイリスデータ
		float x[3];										// 一時的にファイルから読み込んだ値
		for (int i = 0; i < DATA_NUM; i++) {
			fscanf(fp, "%f, %f, %f", &x[0], &x[1], &x[2]);

			flowerLabels[i + (flower * 50)] = flower;

			// ベクトルデータへのポインタを記憶
			sequentialPtr[i + (flower * 50)] = &(X[flower][i]);

			// 花のデータを列ベクトルに代入
			X[flower][i] = cv::Mat(3, 1, CV_32FC1);
			X[flower][i].at<float>(0, 0) = (float)x[0];
			X[flower][i].at<float>(1, 0) = (float)x[1];
			X[flower][i].at<float>(2, 0) = (float)x[2];
		}
		fclose(fp);
	}
}